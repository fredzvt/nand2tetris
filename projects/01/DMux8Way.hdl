// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/DMux8Way.hdl

/**
 * 8-way demultiplexor:
 * {a, b, c, d, e, f, g, h} = {in, 0, 0, 0, 0, 0, 0, 0} if sel == 000
 *                            {0, in, 0, 0, 0, 0, 0, 0} if sel == 001
 *                            etc.
 *                            {0, 0, 0, 0, 0, 0, 0, in} if sel == 111
 */

CHIP DMux8Way {
    IN in, sel[3];
    OUT a, b, c, d, e, f, g, h;

    PARTS:
	
	DMux(in=in, sel=sel[2], a=I, b=II);
	
	DMux(in=I, sel=sel[1], a=III, b=IV);
	DMux(in=II, sel=sel[1], a=V, b=VI);
	
	DMux(in=III, sel=sel[0], a=a, b=b);
	DMux(in=IV, sel=sel[0], a=c, b=d);
	DMux(in=V, sel=sel[0], a=e, b=f);
	DMux(in=VI, sel=sel[0], a=g, b=h);
}
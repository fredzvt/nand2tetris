// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

@screen_value	// This var will hold the value
M=0				// to fill the screen with.
	
(LOOP)

	// ----------------------------------
	// 1: probe kbd and set @screen_value
	// ----------------------------------
	
	@KBD
	D=M		// Store keyboard input value in D
			// D will assume 0 if nothing is pressed
			// and any other number otherwise.
		
	@NO_KEY_PRESSED
	D;JEQ	// if D==0, jump to NO_KEY_PRESSED
	
	// If the jump didn't happend is because some key
	// is pressed in the keyboard.
	
	@screen_value	// Set screen to "white"
	M=-1
	
	@FILL_SCREEN
	0;JMP	// jump to FILL_SCREEN
		
	(NO_KEY_PRESSED)
		
		@screen_value	// Set screen to "black"
		M=0

	(FILL_SCREEN)
		
		// ----------------------------------
		// 1: fill screen with @screen_value
		// ----------------------------------
	
		// Rows: 256 pixels
		// Cols: 32 words (512 pixels)
		// Total: 256 * 32 = 8192 sequential words [16384..24575]
		
		@SCREEN
		D=A
		@screen_reg_addr
		M=D-1				// @screen_reg_addr = @SCREEN - 1
		
		(SCREEN_LOOP)
			@screen_reg_addr
			M=M+1		// screen_reg++
			
			@screen_value
			D=M
			@screen_reg_addr
			A=M
			M=D			// RAM[@screen_reg_addr] = @screen_value
			
			@screen_reg_addr
			D=M
			@24575		// Last screen addr
			D=D-A
			@SCREEN_LOOP
			D;JLT		// if (row - 24576 < 0) jump to SCREEN_LOOP
	
	@LOOP
	0;JMP	// jump to LOOP
	
	
	
	
	
	
	
	
	
	
	
	
	
	
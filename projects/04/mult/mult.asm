// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// ---------------------------
// PSEUDO CODE
// ---------------------------
//
//	sum = 0
//	i = 1
//  n = R0
//	
//	LOOP:
//		if (i > n) goto STOP
//		i++
//		sum = sum + R1
//		goto LOOP
//		
//	STOP:
//		R2 = sum
//		
//	END:
//		infinite loop
	
// ---------------------------
// CODE
// ---------------------------

	@sum
	M=0		// sum = 0

	@i
	M=1		// i = 1
	
	@R0
	D=M
	@n
	M=D		// n = R0

(LOOP)
	@i
	D=M
	@n
	D=D-M
	@STOP
	D;JGT	// if (i > n) goto STOP
	
	@i
	M=M+1	// i++
	
	@sum
	D=M
	@R1
	D=D+M
	@sum
	M=D		// sum = sum + R1
	
	@LOOP
	0;JMP	// goto LOOP
	
(STOP)
	@sum
	D=M
	@R2
	M=D		// R2 = sum
	
(END)
	@END
	0;JMP	// goto END